# Participate

!!! question ""
	What is this and how can I help?

Errors and wishes are collected as *issue* at *gitlab*.
Code and manual are at *gitlab* too.
You can provide changes as *issue* or as *merge request*:

Source Code
:	per git in gitlab unter: https://gitlab.com/open-tt/larc

Manual
:	in gitlab unter: https://open-tt.gitlab.io/larc

Errors
:	in gitlab unter: https://gitlab.com/open-tt/larc/-/issues

Wishes
:	in gitlab unter: https://gitlab.com/open-tt/larc/-/issues

Translations
:	not ready yet...
